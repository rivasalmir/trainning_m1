<?php

    class Foggyline_HappyHour_HelloController extends Mage_Core_Controller_Front_Action{
        public function helloWorldAction(){
            $this->loadLayout();
            $block = $this->getLayout()->createBlock('foggyline_happyhour/hello');
            $block->setText('Hello World #2');
            $this->getLayout()->getBlock('content')->insert($block);
            $this->renderLayout();
        }
    }